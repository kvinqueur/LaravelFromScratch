<div class="col-lg-8">

    <!-- Title -->
    <h1 class="mt-4"><a href="/posts/{{ $post->id }}">{{ $post->title }}</a></h1>
    <!-- Author -->
    <p class="lead">
        by
        <a href="#">{{ $post->user->name }}</a>
    </p>

    <hr>

    <!-- Date/Time -->
    <p>Posted on <time datetime="{{ $post->created_at }}">{{ $post->created_at->diffForHumans() }}</time></p>

    <hr>

    <!-- Preview Image -->
    <img class="img-fluid rounded" src="http://placehold.it/900x300" alt="">

    <hr>

    <!-- Post Content -->
    <p class="lead">{{ $post->body }}</p>
</div>