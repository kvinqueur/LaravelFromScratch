@extends('layouts.master')

@section('content')
    <div class="col-lg-8">

        <!-- Title -->
        <h1 class="mt-4">{{ $post->title }}</h1>

        @if (count($post->tags))
            <ul>
                @foreach($post->tags as $tag)
                    <li><a href="/posts/tags/{{ $tag->name }}">{{ $tag->name }}</a></li>
                @endforeach
            </ul>
    @endif

        <!-- Author -->
        <p class="lead">
            by
            <a href="#">Start Bootstrap</a>
        </p>

        <hr>

        <!-- Date/Time -->
        <p>Posted on <time datetime="{{ $post->created_at }}">{{ $post->created_at->diffForHumans() }}</time></p>

        <hr>

        <!-- Preview Image -->
        <img class="img-fluid rounded" src="http://placehold.it/900x300" alt="">

        <hr>

        <!-- Post Content -->
        <p class="lead">{{ $post->body }}</p>

        <hr>

        <!-- Comment with nested comments -->
        <div class="media mb-4">
            <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">
            <div class="media-body">
                <h5 class="mt-0">Commenter Name</h5>
                @foreach($post->comments as $comment)
                    <div class="media mt-4">
                        <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">
                        <div class="media-body">
                            <h5 class="mt-0">Commenter Name</h5>
                            {{ $comment->body }}
                        </div>
                        <div class="media-date">
                            <span>Created At: </span>{{ $comment->created_at->diffForHumans() }}
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <hr>

        <div class="card">
            <div class="card-block">
                <form method="POST" action="/posts/{{ $post->id }}/comments">

                    {{ csrf_field() }}
                    <div class="form-group">
                        <textarea name="body" id="body" placeholder="Add your text here." class="form-control"></textarea>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Add comment</button>
                    </div>

                    @include('layouts.errors')
                </form>
            </div>
        </div>
    </div>
@endsection