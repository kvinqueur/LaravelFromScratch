<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Laravel From Scratch">
    <meta name="author" content="Kim Vinqueur">

    <title>Blog Post - Start Bootstrap Template</title>

    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/css/blog-post.css" rel="stylesheet">

</head>

<body>

@include('layouts.nav')

@if ($flash = session('message'))
    <div id="flash-message" class="alert alert-success" role="alert">
        {{ $flash }}
    </div>
@endif

<!-- Page Content -->
<div class="container">
    <div class="row">
        @yield('content')

        @include('layouts.sidebar')
    </div>
</div>
<!-- /.container -->

@include('layouts.footer')

</body>

</html>