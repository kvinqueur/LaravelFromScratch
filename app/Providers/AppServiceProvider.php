<?php

namespace App\Providers;

use App\Billing\Stripe;
use App\Post;
use App\Tags;
use Illuminate\Support\ServiceProvider;
use function compact;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('layouts.sidebar', function ($view) {
            $archives = Post::archives();
            $tags = Tags::has('post')->pluck('name');
            $view->with(compact(['archives', 'tags']));
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Stripe::class, function () {
            return new Stripe(config('services.stripe.secret'));
        });
    }
}
