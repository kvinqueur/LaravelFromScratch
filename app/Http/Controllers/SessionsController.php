<?php

namespace App\Http\Controllers;


class SessionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest')->except(['destroy']);
    }

    public function create()
    {
        return view('sessions.create');
    }

    public function store()
    {
        // Attempt to authenticate the user
        if (! auth()->attempt(request(['email', 'password']))) {
            // If not, redirect back
            return back()->withErrors([
                'message' => 'Please check your credentials and try again.'
            ]);
        }
        // Redirect to the home page.
        return redirect()->home();
    }

    public function destroy()
    {
        // Logout the user
        auth()->logout();

        // Redirect to the home page.
        return redirect()->home();
    }
}
