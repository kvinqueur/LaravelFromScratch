<?php

namespace App\Http\Controllers;

use App\Post;
use App\Tags;
use function session;

class PostsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth', ['except' => 'index']);
    }

    public function index(Tags $tag)
    {
        $posts = Post::latest();

        if (request(['month', 'year'])) {
            $posts->filter(request(['month', 'year']));
        }

        $posts = $posts->get();

        return view('posts.index', compact('posts'));
    }

    public function show(Post $post)
    {
        return view('posts.show', compact('post'));
    }

    public function create()
    {
        return view('posts.create');
    }

    public function store()
    {
        $this->validate(request(), [
            'title' => 'required',
            'body' => 'required',
        ]);

        auth()->user()->publish(
            new Post(request(['title', 'body']))
        );

        session()->flash('message', 'Votre article a bien ete publie.');

//        Post::create([
//            'title' => request('title'),
//            'body' => request('body'),
//            'user_id' => auth()->id(),
//        ]);

        return redirect()->home();
    }
}
