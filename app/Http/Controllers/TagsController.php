<?php

namespace App\Http\Controllers;

use App\Tags;

class TagsController extends Controller
{

    public function index(Tags $tag)
    {
        $posts = $tag->post;

        return view('posts.index', compact('posts'));
    }
}
