<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegistrationForm;
use function session;

class RegistrationController extends Controller
{

    public function create()
    {
        return view('registration.create');
    }

    public function store(RegistrationForm $form)
    {
        $form->persist();

        session()->flash('message', 'Merci de vous etre inscrit sur notre site web.');

        // Redirect to the home page.
        return redirect()->home();
        // return redirect('/');
    }
}
