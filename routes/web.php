<?php

// Lesson 24

//App::singleton('App\Billing\Stripe', function () {
//   return new App\Billing\Stripe(config('services.stripe.secret'));
//});

// App::bind('App\Billing\Stripe', function () {
//    return new App\Billing\Stripe(config('services.stripe.secret'));
// });

//$stripe = App::make('App\Billing\Stripe');
//$stripe = resolve('App\Billing\Stripe');

//dd($stripe);

// Lessons 1-9

Route::get('/tasks', 'TasksController@index');

Route::get('/tasks/{task}', 'TasksController@show');

// Lesson 10
Route::get('/', 'PostsController@index')->name('home');

// Lesson 11
Route::get('/posts/create', 'PostsController@create');
Route::post('/posts', 'PostsController@store');

// Lesson 12
Route::get('/posts/{post}', 'PostsController@show');

// Lesson 16
Route::post('/posts/{post}/comments', 'CommentsController@store');

//Lessons 18 - 19
Route::get('/register', 'RegistrationController@create');
Route::post('/register', 'RegistrationController@store');
Route::get('/login', 'SessionsController@create');
Route::post('/login', 'SessionsController@store');
Route::get('/logout', 'SessionsController@destroy');

// Lesson 31
Route::get('/posts/tags/{tag}', 'TagsController@index');
